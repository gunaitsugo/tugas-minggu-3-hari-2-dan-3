<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Events\UserRegisteredEvent;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'name' => ['string', 'required'],
            'username' => ['alpha_num', 'required', 'min:3', 'max:25', 'unique:users,username'],
            'email' => ['email', 'required', 'unique:users,email'],
            'password' => ['required', 'min:6']
        ]);

        $daftar = new User();
        $daftar->name = $request->name;
        $daftar->username = $request->username;
        $daftar->email = $request->email;
        $daftar->password = bcrypt($request->password);
        $daftar->save();

        event(new UserRegisteredEvent($daftar));

        if ($daftar) {
            return response()->json([
                'success' => true,
                'data' => $daftar
            ], 200);
        }
        
    }
}
