<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use App\Http\Resources\MahasiswaCollection;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $mahasiswa = Mahasiswa::paginate(2);

        return new MahasiswaCollection($mahasiswa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'nim' => ['required'],
            'fakultas' => ['required'],
            'jurusan' => ['required'],
            'no_hp' => ['required'],

        ]);

        $mahasiswa = auth()->user()->mahasiswa()->create([
            'nama' => request('nama'),
            'nim' => request('nim'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan'),
            'no_hp' => request('no_hp'),
            'no_wa' => request('no_wa'),
        ]);
        
        return $mahasiswa;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mahasiswa $mahasiswa)
    {
        return $mahasiswa;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Mahasiswa $mahasiswa)
    {
        $data = $request->all();
        $mahasiswa->update($data);
        return $mahasiswa;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mahasiswa $mahasiswa)
    {
        $mahasiswa->delete();
        return response()->json('Data sudah dihapus', 200);
    }
}
