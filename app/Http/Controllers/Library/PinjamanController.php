<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pinjaman;
use App\Http\Resources\PinjamanCollection;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pinjaman = Pinjaman::paginate(2);

        return new PinjamanCollection($pinjaman);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'by_kode_buku' => ['required', 'min:5', 'max:7'],
            'tgl_peminjaman' => ['required'],
            'batas_peminjaman' => ['required'],
            'tgl_pengembalian' => ['required'],

        ]);

        $pinjams = auth()->user()->pinjaman()->create([
            'by_kode_buku' => request('by_kode_buku'),
            'tgl_peminjaman' => request('tgl_peminjaman'),
            'batas_peminjaman' => request('batas_peminjaman'),
            'tgl_pengembalian' => request('tgl_pengembalian'),
        ]);
        
        return $pinjams;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pinjaman $pinjaman)
    {
        return $pinjaman;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Pinjaman $pinjaman)
    {
        $data = $request->all();
        $pinjaman->update($data);
        return $pinjaman;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pinjaman $pinjaman)
    {
        $pinjaman->delete();
        return response()->json('Data sudah dihapus', 200);
    }
}
