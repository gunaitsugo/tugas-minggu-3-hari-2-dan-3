<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';
    protected $guarded = [];

    public function getRouteKeyName(){
        return 'kode_buku';
    }

}
