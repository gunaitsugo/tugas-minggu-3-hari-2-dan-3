<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    protected $guarded = [];

    public function getRouteKeyName(){
        return 'user_id';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
