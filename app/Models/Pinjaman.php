<?php

namespace App\Models;

use App\User;
use App\Model\Buku;
use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    protected $table = 'pinjaman';
    protected $guarded = [];

    public function getRouteKeyName(){
        return 'id';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function buku()
    {
        return $this->belongsTo(Buku::class);
    }

}
