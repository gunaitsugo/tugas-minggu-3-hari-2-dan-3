<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pinjaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjaman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('by_kode_buku');
            $table->unsignedBigInteger('user_id');
            $table->string('tgl_peminjaman');
            $table->string('batas_peminjaman');
            $table->date('tgl_pengembalian');
            $table->boolean('status_ontime');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('by_kode_buku')->references('kode_buku')->on('buku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
