<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Auth')->group(function(){
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
});

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('profil', 'UserController');
});

Route::namespace('Library')->group(function(){
    Route::post('create-buku', 'BukuController@store');
    Route::patch('update-buku/{buku}', 'BukuController@update');
    Route::delete('delete-buku/{buku}', 'BukuController@destroy');
    Route::get('buku/{buku}', 'BukuController@show');
    Route::get('buku', 'BukuController@index');
});

Route::namespace('Library')->middleware('auth.jwt')->group(function(){
    Route::post('create-mahasiswa', 'MahasiswaController@store');
    Route::post('create-pinjaman', 'PinjamanController@store');
    
    Route::get('mahasiswa/{mahasiswa}', 'MahasiswaController@show');
    Route::get('mahasiswa', 'MahasiswaController@index');
    Route::patch('update-mahasisa/{mahasiswa}', 'MahasiswaController@update');
    Route::delete('delete-mahasiswa/{mahasiswa}', 'MahasiswaController@destroy');
    
    Route::get('pinjaman/{pinjaman}', 'PinjamanController@show');
    Route::get('pinjaman', 'PinjamanController@index');
    Route::patch('update-pinjaman/{pinjaman}', 'PinjamanController@update');
    Route::delete('delete-pinjaman/{pinjaman}', 'PinjamanController@destroy');
    
});

Route::get('user', 'UserController');